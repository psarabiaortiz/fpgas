--------------------------------------------------------------------------------
--
-- Title       : 	TestBench Problema 1 parcial 21-22
-- Design      :	
-- Author      :	Pablo Sarabia Ortiz
-- Company     :	Universidad de Nebrija
--------------------------------------------------------------------------------
-- File        : tb_Problema1.vhd
-- Generated   : 18 March 2022
--------------------------------------------------------------------------------
-- Description : Problema 1 examen Parcial
-- Enunciado   :
-- Se desea implementar un sistema que cuando se pulse el botón (BTN = ‘1’) 
-- cuente 10 ciclos de reloj, si el botón sigue pulsado, al acabar de contar, 
-- se debe poner la salida a 1 (OUT = ‘1’ ) durante los ciclos que el botón siga 
-- pulsado. Una vez el botón se despulse (BTN = ‘0’) la salida se debe poner a 0. 
-- El sistema cuenta con una entrada de enable que si está a 1 (EN = ‘1’) funciona 
-- con normalidad y si está a 0 el sistema no hace nada. El sistema debe contar 
-- con un reset (RST) síncrono a nivel bajo
--------------------------------------------------------------------------------
-- Revision History :
-- -----------------------------------------------------------------------------

--   Ver  :| Author            :| Mod. Date :|    Changes Made:

--   v1.0  | Pablo Sarabia     :| 18/03/22  :| First version

-- -----------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity tb_Problema1 is
end tb_Problema1;

architecture testBench of tb_Problema1 is 
	component problema1 is 
		port(
		EN, BTN, RST, CLK 	: in  std_logic;
		OUT1 				: out std_logic
		);
	end component;
	constant period : time := 20 ns;
	signal EN, BTN, RST, OUT1: std_logic;
	signal CLK : std_logic := '0';
	
begin
	DUT : problema1
	port map (
		EN 		=> EN,
		BTN 	=> BTN,
		RST 	=> RST,
		CLK 	=> CLK,
		OUT1	=> OUT1
	);
	CLK <= not CLK after period/2;
	process is 
	begin 
		EN 	<= '0';	
		BTN <= '0';
		RST <= '0';
		wait until rising_edge(CLK);
		RST <= '1';
		wait until rising_edge(CLK);
		EN 	<= '1';	
		BTN <= '0';
		wait until rising_edge(CLK);
		BTN <= '1';
		for i in 0 to 9 loop
			wait until rising_edge(CLK);
		end loop;
		BTN <= '0';
		wait until rising_edge(CLK);
		BTN <= '1';
		for i in 0 to 20 loop
			wait until rising_edge(CLK);
		end loop;
	end process;	
end testBench;