--------------------------------------------------------------------------------
--
-- Title       : 	Problema 1 parcial 21-22
-- Design      :	
-- Author      :	Pablo Sarabia Ortiz
-- Company     :	Universidad de Nebrija
--------------------------------------------------------------------------------
-- File        : Problema1.vhd
-- Generated   : 18 March 2022
--------------------------------------------------------------------------------
-- Description : Problema 1 examen Parcial
-- Enunciado   :
-- Se desea implementar un sistema que cuando se pulse el botón (BTN = ‘1’) 
-- cuente 10 ciclos de reloj, si el botón sigue pulsado, al acabar de contar, 
-- se debe poner la salida a 1 (OUT = ‘1’ ) durante los ciclos que el botón siga 
-- pulsado. Una vez el botón se despulse (BTN = ‘0’) la salida se debe poner a 0. 
-- El sistema cuenta con una entrada de enable que si está a 1 (EN = ‘1’) funciona 
-- con normalidad y si está a 0 el sistema no hace nada. El sistema debe contar 
-- con un reset (RST) síncrono a nivel bajo
--------------------------------------------------------------------------------
-- Revision History :
-- -----------------------------------------------------------------------------

--   Ver  :| Author            :| Mod. Date :|    Changes Made:

--   v1.0  | Pablo Sarabia     :| 18/03/22  :| First version

-- -----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity problema1 is 
	port(
		EN, BTN, RST, CLK 	: in  std_logic;
		OUT1 				: out std_logic
	);
end problema1;

architecture rtl of problema1 is
	signal count : unsigned(3 downto 0);
	signal en_count, finished : std_logic;
	type t_states is (IDLE, COUNTING, VALID);
	signal current_state, next_state: t_states;
	
	constant c_max_cycles : unsigned(3 downto 0) := to_unsigned(10, count'length);
begin

counter: process(CLK)
begin
	if rising_edge(CLK) then
		if RST = '0' then
			count <= (others => '0');
			finished <= '0';
		else
			count <= (others => '0');

			if en_count = '1' then
				if count < c_max_cycles - 2 then 
				-- It's necesary to compare with 2 less 
				--than the desired cycles so the delay 
				-- inserted by the register is accounted
					count <= count +1;
					finished <= '0';
				else
					finished <= '1';
					count <= ( others=> '0');
                end if;

			end if;
		end if;
	end if;
end process;

fsm_register: process(CLK)
begin
	if rising_edge(CLK) then
		if RST = '0' then
			current_state <= IDLE;
		else
			current_state <= next_state;
		
		end if;
	end if;
end process;

fsm_comb: process(current_state, EN, BTN, finished)
begin
	if EN = '0' then
		next_state <= IDLE;
		OUT1 <= '0';
		en_count <= '0';
	else
		next_state <= current_state;
		case current_state is
			when IDLE =>
				OUT1 <= '0';
				en_count <= '0';
				if BTN = '1' then
					next_state <= COUNTING;
					en_count <= '1';
				end if;
			when COUNTING => 
				OUT1 <= '0';
				en_count <= '1';
				if finished = '1' then
					if BTN = '1' then
					    OUT1 <= '1';
						next_state <= VALID;
					else
						next_state <= IDLE;
					end if;
				end if;
			when VALID =>
				OUT1 <= '1';
				en_count <= '0';
				if BTN = '0' then
					OUT1 <= '0';
					next_state <= IDLE;
				end if;
		end case;
	end if;

end process;
end rtl;