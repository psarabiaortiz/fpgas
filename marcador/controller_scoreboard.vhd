--------------------------------------------------------------------------------
--
-- Title       : 	Controller Scoreboard
-- Design      :	
-- Author      :	Pablo Sarabia Ortiz
-- Company     :	Universidad de Nebrija
--------------------------------------------------------------------------------
-- File        : controller_scoreboard.vhd
-- Generated   : 9 February 2022
--------------------------------------------------------------------------------
-- Description : 
--------------------------------------------------------------------------------
-- Revision History :
-- -----------------------------------------------------------------------------

--   Ver  :| Author            :| Mod. Date :|    Changes Made:

--   v1.0  | Pablo Sarabia     :| 09/02/22  :| First version

-- -----------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Scoreboard is
port(clk, rst, inc, dec, n_rst: in std_logic;
	seg7disp1, seg7disp0: out std_logic_vector(6 downto 0)
	);
end Scoreboard;

architecture Behavioral of Scoreboard is
	type fsm_state is (CLEAR, COUNT);
	
	signal state, stateNext				: fsm_state;
	signal BCD1, BCD0, r_BCD1, r_BCD0	: unsigned(3 downto 0) := "0000"; -- unsigned bit vector
	signal rstcnt, r_rstcnt		: unsigned(3 downto 0); --Up to 5
	
	type sevsegarray is array (0 to 9) of std_logic_vector(6 downto 0);
	constant seg7Rom: sevsegarray :=
		("0111111", "0000110", "1011011", "1001111", "1100110", "1101101", "1111100",
		"0000111", "1111111", "1100111"); -- active high with "gfedcba" order

begin
	process (clk, n_rst)
	begin
		if n_rst = '1' then
			state    <= CLEAR;
            r_BCD0   <= ( others=> '0');
            r_BCD1   <= ( others=> '0');
            r_rstcnt <= ( others=> '0');
		elsif rising_edge(clk)then
			state    <= stateNext;
			r_BCD0   <= BCD0;
			r_BCD1   <= BCD1;
			r_rstcnt <= rstcnt;
		end if;
	end process;
	process(state, r_BCD0, r_BCD1, r_rstcnt, inc, dec)
	begin
			stateNext <= state;
			case state is
				when CLEAR => -- initial state
					BCD1 <= "0000"; BCD0 <= "0000"; -- clear counter
					rstcnt <= (others => '0'); -- reset RESETCOUNT
					stateNext <= COUNT;
				when COUNT => -- state in which the scoreboard waits for inc and dec
					if rst = '1' then
						if rstcnt < to_unsigned(5, rstcnt'length) then -- checking whether 5th reset cycle
							rstcnt <= r_rstcnt + 1;							
						else 
							stateNext <= CLEAR;
							rstcnt <= (others => '0');
						end if;
					elsif inc = '1' and dec = '0' then
							rstcnt <= (others => '0');
						if BCD0 < to_unsigned(9, BCD0'length) then
							BCD0 <= r_BCD0 + 1; -- library with overloaded "+" required
						elsif BCD1 < to_unsigned(9, BCD1'length) then
							BCD1 <= r_BCD1 + 1;
							BCD0 <= (others => '0');
						end if;
					elsif dec = '1' and inc = '0' then
						rstcnt <= (others => '0');
						if BCD0 > to_unsigned(0, BCD0'length) then
							BCD0 <= r_BCD0 - 1; -- library with overloaded "-" required
						elsif BCD1 > to_unsigned(0, BCD1'length) then
							BCD1 <= r_BCD1 - 1;
							BCD0 <= "1001";
						end if;
					elsif (inc = '1' and dec = '1') or (inc = '0' and dec = '0') then
						rstcnt <= (others => '0');
					end if;
				when others =>
				    stateNext <= CLEAR;
			end case;
	end process;
	seg7disp0 <= seg7rom(to_integer(BCD0)); -- type conversion function from
	seg7disp1 <= seg7rom(to_integer(BCD1)); -- IEEE numeric_bit package used
end Behavioral;