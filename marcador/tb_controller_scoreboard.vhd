--------------------------------------------------------------------------------
--
-- Title       : 	Testbench for the controller testBench
-- Design      :	
-- Author      :	Pablo Sarabia Ortiz
-- Company     :	Universidad de Nebrija
--------------------------------------------------------------------------------
-- File        : tb_controller_scoreboard.vhd
-- Generated   : 9 February 2022
--------------------------------------------------------------------------------
-- Description : 
--------------------------------------------------------------------------------
-- Revision History :
-- -----------------------------------------------------------------------------

--   Ver  :| Author            :| Mod. Date :|    Changes Made:

--   v1.0  | Pablo Sarabia     :| 09/02/22  :| First version

-- -----------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity tb_debouncer is
end tb_debouncer;

architecture testBench of tb_debouncer is
	component Scoreboard is
		port(clk, rst, inc, dec, n_rst: in std_logic;
			seg7disp1, seg7disp0: out std_logic_vector(6 downto 0)
		);
	end component;
	
	signal clk, rst, inc, dec, n_rst :std_logic:= '0';
	signal seg7disp1, seg7disp0: std_logic_vector(6 downto 0);
	constant clk_period : time := 20ns;
begin	
	UUT : Scoreboard 
	port map (
		clk			=>	clk			,
		rst         =>	rst         ,
		inc         =>	inc         ,
		dec         =>	dec         ,
		n_rst       =>	n_rst       ,
		seg7disp1   =>	seg7disp1   ,
		seg7disp0   =>	seg7disp0   
	);
	clk <= not clk after clk_period/2;
	process is 
	begin
	wait until rising_edge(clk);
    wait until rising_edge(clk);
    n_rst <= '1';
    wait until rising_edge(clk);
	n_rst <= '0';
	wait until rising_edge(clk);
	rst <= '0';
	inc <= '1';
	dec <= '0';
	wait until rising_edge(clk);
	rst <= '0';
	inc <= '1';
	dec <= '0';
	wait until rising_edge(clk);
	rst <= '0';
	inc <= '0';
	dec <= '1';
	wait until rising_edge(clk);
	rst <= '0';
	inc <= '1';
	dec <= '1';
	wait until rising_edge(clk);
	rst <= '0';
	inc <= '1';
	dec <= '0';
	wait until rising_edge(clk);
	rst <= '1';
	inc <= '0';
	dec <= '0';
	for I in 0 to 5 loop
		wait until rising_edge(clk);
	end loop;
end process;
end testBench;