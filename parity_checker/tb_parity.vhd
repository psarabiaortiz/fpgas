--------------------------------------------------------------------------------
--
-- Title       : 	Testbench for Parity checker with genric size and type of parity
-- Design      :	
-- Author      :	Pablo Sarabia Ortiz
-- Company     :	Universidad de Nebrija
--------------------------------------------------------------------------------
-- File        : tb_parity.vhd
-- Generated   : 06 April 2022
--------------------------------------------------------------------------------
-- Description : Parity Checker
-- Checks the parity generic to set the size of the word and the type of parity
-- Definition of odd parity: The number of '1' in the word plus the parity bit 
--								adds to an odd number
-- Definition even: The number of '1' in the word plus the parity bit adds to an
--						even number.		
--------------------------------------------------------------------------------
-- Revision History :
-- -----------------------------------------------------------------------------

--   Ver  :| Author            :| Mod. Date :|    Changes Made:

--   v1.0  | Pablo Sarabia     :| 06/04/22  :| First version

-- -----------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity tb_parity is

end tb_parity;

architecture tb of tb_parity is
component parity_checker is
	generic(
		g_size_word		: integer := 8; --Number of bytes to check parity
		g_parity_type	: std_logic := '0' -- '0' for even '1' for odd
	);
	port(
		input_bits 	: in std_logic_vector(g_size_word-1 downto 0);
		parity 		: out std_logic
	);
end component;
constant c_size_word :integer := 8;
constant c_parity: std_logic := '0';
signal inputWord : std_logic_vector(c_size_word-1 downto 0);
signal result : std_logic ;

begin
UUT: parity_checker 
    generic map(
        g_size_word => c_size_word ,
        g_parity_type => c_parity 
    )
    port map(
    input_bits => inputWord,
    parity => result
    );
    process is
    begin
        wait for 10 ns;
        inputWord <= "10101010";
        wait for 10 ns;
        inputWord <= "10101011";
        wait for 10 ns;
        
    end process;
    
    


end tb;
