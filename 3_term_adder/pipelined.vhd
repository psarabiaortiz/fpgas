--------------------------------------------------------------------------------
--
-- Title       : 	Loop Folding Example
-- Design      :	
-- Author      :	Pablo Sarabia Ortiz
-- Company     :	Universidad de Nebrija
--------------------------------------------------------------------------------
-- File        : pipelined.vhd
-- Generated   : 3 May 2022
--------------------------------------------------------------------------------
-- Description : Pipelined implementation y(n) = a(n)+b(n)+c(n) pipeline in between 
-- a+b and c
--					
--------------------------------------------------------------------------------
-- Revision History :
-- -----------------------------------------------------------------------------

--   Ver  :| Author            :| Mod. Date :|    Changes Made:

--   v1.0  | Pablo Sarabia     :| 03/05/22  :| First version

-- -----------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity pipelined is
	port(
		rst_n	: in	std_logic;
		clk		: in	std_logic;
		a, b, c : in  	std_logic_vector(7 downto 0);
		output	: out	std_logic_vector(9 downto 0)
	);
end pipelined;

architecture rtl of pipelined is 
signal ab : signed(8 downto 0);
signal reg_ab : signed(8 downto 0);
signal reg_c : std_logic_vector (7 downto 0);
begin
process(clk)
begin
if rising_edge(clk) then
	if rst_n = '0' then
		reg_ab	<= (others=>'0');
		reg_c	<= (others=>'0');
	else
		reg_ab 	<= ab;
		reg_c	<= c;
	end if;
end if;
end process;
ab <= resize(signed(a) , ab'length)+ resize(signed(b), ab'length);
output <= std_logic_vector(resize(reg_ab, output'length) + resize(signed(reg_c), output'length));

end architecture;