--------------------------------------------------------------------------------
--
-- Title       : 	Loop Folding Example
-- Design      :	
-- Author      :	Pablo Sarabia Ortiz
-- Company     :	Universidad de Nebrija
--------------------------------------------------------------------------------
-- File        : loopFolding.vhd
-- Generated   : 3 May 2022
--------------------------------------------------------------------------------
-- Description : Example of how to fold the following function y(n) = a(n)+b(n)+c(n)
--					by using only one adder.
--------------------------------------------------------------------------------
-- Revision History :
-- -----------------------------------------------------------------------------

--   Ver  :| Author            :| Mod. Date :|    Changes Made:

--   v1.0  | Pablo Sarabia     :| 03/05/22  :| First version

-- -----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity foldedFunction is
	port(
		rst_n	: in	std_logic;
		clk		: in	std_logic;
		start	: in 	std_logic;
		valid	: out 	std_logic;
		a, b, c : in  	std_logic_vector(7 downto 0);
		output	: out	std_logic_vector(9 downto 0)
	);
end foldedFunction;

architecture rtl of foldedFunction is 
	signal cnt : unsigned(1 downto 0);
	signal reg_c: signed(7 downto 0);
	signal ab, reg_ab, add_a, add_b : signed(8 downto 0);
	signal add_out : signed(9 downto 0); --Max size adder to don't have overflow
begin
	add_out <= resize(add_a, add_out'length) + resize(add_b, add_out'length); -- The adder always has 
	--the same signals and with a mux we change the input according with the cycle
counter: process(clk)
begin
	if rising_edge(clk) then
		if rst_n = '0' then
			cnt <= (others => '0');
		elsif start = '1'then
			if cnt < 1 then -- Always iterating between the two states
				cnt <= cnt + 1;
			else
				cnt <= (others => '0');
			end if;
		else
				cnt <= (others => '0');
		end if;
	end if;
end process;

adder: process(cnt, a, b, add_out, reg_ab, c)
begin
	case cnt is
		when "00" => -- reg_ab = a+b;
			add_a <= resize(signed(a), add_a'length);
			add_b <= resize(signed(b), add_b'length);
			ab <= resize(add_out, reg_ab'length); --going to bigger size to smaller works because the MSB should be empty
			output <= (others=> '0');
			valid <= '0';
		when "01" => -- out = reg_ab+c;
			add_a <= reg_ab;
			add_b <= resize(reg_c, add_b'length);
			output<= std_logic_vector(add_out);
			valid <= '1';
		when others=>
			add_a <= (others => '0');
			add_b <= (others => '0');
			output <= (others=> '0');
			valid <= '0';
			
	end case;
end process; 
registers: process(clk)
begin
	if rising_edge(clk) then
		if rst_n = '0' then
			reg_ab 	<= (others => '0');
			reg_c 	<= (others => '0');
		else
			reg_ab 	<= ab;
			reg_c 	<= signed(c);
		end if;
	end if;
	
end process;
end architecture;