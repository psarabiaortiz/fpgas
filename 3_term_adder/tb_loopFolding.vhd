--------------------------------------------------------------------------------
--
-- Title       : 	Loop Folding Example
-- Design      :	
-- Author      :	Pablo Sarabia Ortiz
-- Company     :	Universidad de Nebrija
--------------------------------------------------------------------------------
-- File        : tb_loopFolding.vhd
-- Generated   : 3 May 2022
--------------------------------------------------------------------------------
-- Description : Example of how to fold the following function y(n) = a(n)+b(n)+c(n)
--					by using only one adder.
--------------------------------------------------------------------------------
-- Revision History :
-- -----------------------------------------------------------------------------

--   Ver  :| Author            :| Mod. Date :|    Changes Made:

--   v1.0  | Pablo Sarabia     :| 03/05/22  :| First version

-- -----------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity tb_loopFolding is 
end tb_loopFolding;

architecture tb of tb_loopFolding is
component foldedFunction is
	port(
		rst_n	: in	std_logic;
		clk		: in	std_logic;
		start	: in 	std_logic;
		valid	: out 	std_logic;
		a, b, c : in  	std_logic_vector(7 downto 0);
		output	: out	std_logic_vector(9 downto 0)
	);
end component;
signal a, b, c: signed( 7 downto 0):= (others=>'0');
signal sum: signed(9 downto 0);
signal clk, rst_n, start, valid: std_logic:= '0';
constant clk_period: time:= 20ns;
 
begin
UUT: foldedFunction
port map(
	rst_n => rst_n,
	clk => clk,
	start => start,
	valid => valid,
	a => std_logic_vector(a),
	b => std_logic_vector(b),
	c => std_logic_vector(c),
	signed(output) => sum
	);
clk <= not clk after clk_period/2;
process is 
begin
	wait until rising_edge(clk);
	wait until rising_edge(clk);
	rst_n <= '0';
	wait until rising_edge(clk);
	rst_n <= '1';	
	wait until rising_edge(clk);
	start <= '1';
	a <= to_signed(2, a'length);
	b <= to_signed(3, a'length);
	c <= to_signed(4, a'length);
	wait until rising_edge(clk);
    a <= to_signed(-127, a'length);
	b <= to_signed(126, a'length);
	c <= to_signed(44, a'length);
	wait until rising_edge(clk);
	a <= to_signed(127, a'length);
	b <= to_signed(127, a'length);
	c <= to_signed(127, a'length);
	wait until rising_edge(clk);
end process;
end architecture;