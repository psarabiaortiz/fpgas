--------------------------------------------------------------------------------
--
-- Title       : 	Loop Folding Example
-- Design      :	
-- Author      :	Pablo Sarabia Ortiz
-- Company     :	Universidad de Nebrija
--------------------------------------------------------------------------------
-- File        : direct.vhd
-- Generated   : 3 May 2022
--------------------------------------------------------------------------------
-- Description : Direct implementation y(n) = a(n)+b(n)+c(n)
--					
--------------------------------------------------------------------------------
-- Revision History :
-- -----------------------------------------------------------------------------

--   Ver  :| Author            :| Mod. Date :|    Changes Made:

--   v1.0  | Pablo Sarabia     :| 03/05/22  :| First version

-- -----------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity direct is
	port(
		rst_n	: in	std_logic;
		clk		: in	std_logic;
		a, b, c : in  	std_logic_vector(7 downto 0);
		output	: out	std_logic_vector(9 downto 0)
	);
end direct;

architecture rtl of direct is 
signal ab : signed(8 downto 0);
begin

ab <= resize(signed(a) , ab'length)+ resize(signed(b), ab'length);
output <= std_logic_vector(resize(ab, output'length) + resize(signed(c), output'length));

end architecture;