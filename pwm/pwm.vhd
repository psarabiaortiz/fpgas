--------------------------------------------------------------------------------
--
-- Title       : 	Problema 1.7: PWM controller
-- Design      :	
-- Author      :	Pablo Sarabia Ortiz
-- Company     :	Universidad de Nebrija
--------------------------------------------------------------------------------
-- File        : pwm.vhd
-- Generated   : 13 February 2022
--------------------------------------------------------------------------------
-- Description : circuito PWM que mediante una señal ancho ‘W’(de 4 bits) permita
-- seleccionar el ciclo de trabajo a 0000 el ciclo de trabajo es 100% y en cualquier otro
-- caso es W/16. Se debe incluir un reset síncrono.
--  
--------------------------------------------------------------------------------
-- Revision History :
-- -----------------------------------------------------------------------------

--   Ver  :| Author            :| Mod. Date :|    Changes Made:

--   v1.0  | Pablo Sarabia     :| 28/02/22  :| First version

-- -----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pwm is
   port(
      clk, reset: in std_logic;
      w: in std_logic_vector(3 downto 0);
      pwm_pulse: out std_logic
   );
end pwm;

architecture two_seg_arch of pwm is
   signal r_reg: unsigned(3 downto 0);
   signal buf_reg: std_logic;
   signal buf_next: std_logic;
begin
   -- register & output buffer
   process(clk,reset)
   begin
      if (reset='1') then
         r_reg <= (others=>'0');
         buf_reg <= '0';
      elsif (rising_edge(clk)) then
	  -- The counter will overflow and take the 0 value
         r_reg <= r_reg + 1; 
         buf_reg <= buf_next;
      end if;
   end process;
   
   -- output logic
   buf_next <=
       '1' when (r_reg<unsigned(w)) or (w="0000") else
       '0';
   pwm_pulse <= buf_reg;
end two_seg_arch;