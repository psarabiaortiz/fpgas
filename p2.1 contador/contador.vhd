--------------------------------------------------------------------------------
--
-- Title       : 	Problema 2.1: Contador
-- Design      :	
-- Author      :	Pablo Sarabia Ortiz
-- Company     :	Universidad de Nebrija
--------------------------------------------------------------------------------
-- File        : contador.vhd
-- Generated   : 13 February 2022
--------------------------------------------------------------------------------
-- Description : Contador síncrono que realiza la siguiente secuencia 
-- 1000, 0111, 0110, 0101, 0100, 0011, sólo cuando enable está a 1
-- LD carga el valor de 1000
-- RST asíncrono a nivel bajo (salida END a 0 y Q a 1000)
--  
--------------------------------------------------------------------------------
-- Revision History :
-- -----------------------------------------------------------------------------

--   Ver  :| Author            :| Mod. Date :|    Changes Made:

--   v1.0  | Pablo Sarabia     :| 28/02/22  :| First version

-- -----------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
-- Declaraci�n de la entidad
entity contador is 
port (clk, n_rst, enable, LD: in std_logic;
	finish: out std_logic;
	Q: out std_logic_vector (3 downto 0)
);
end contador;

architecture behav of contador is
-- Valor para precargar el contador
constant c_QPreload : unsigned(2 downto 0) := to_unsigned(0, 3); -- Indice con el valor de la precarga
-- Tipo de datos para hacer el array del contador
type counter_values_type is array (0 to 5) of std_logic_vector (3 downto 0);
-- Valores del contador
constant c_values : counter_values_type := ("1000", "0111", "0110", "0101", "0100", "0011");
-- Indice que lleva la cuenta
signal CNT, CNT_async : unsigned (2 downto 0); -- Para contar hasta 6

begin
-- Proceso registrado(s�ncrono) para incrementar el contador
process (clk, n_rst)
begin
	if n_rst = '0' then
	-- Reset as�ncrono a nivel bajo
		CNT <= c_QPreload;
	elsif rising_edge(clk) then	
	-- El contador est� registrado, las salidas van a cambiar con un ciclo de retraso	
		CNT <= CNT_async;		
	end if;
end process;
-- proceso para la m�quina de estados que cuenta
process (LD, enable, CNT)
begin
	if LD = '1' then
		CNT_async <= c_QPreload; 
	elsif enable = '1' then
		if CNT = to_unsigned(5, CNT'length) then
			CNT_async <= to_unsigned(0, CNT_async'length);
		else
		CNT_async <= CNT +1;
		end if;
	else
		CNT_async <= CNT;
	end if;
end process;

-- proceso para la se�al de salida finish
process (CNT)
begin 
if CNT = to_unsigned(3, CNT'length ) then -- Variable hardcodeada(indice para el contador = 0101) se deber�a sustituir por una constante
    finish <= '1'; -- La variable es as�ncrona pero toma el valor seg�n variable registrada CNT
else
    finish <= '0';
end if;
end process;
Q <= c_values(to_integer(CNT)); -- La salida es combinacional con el array de constantes del contador
end behav;