--------------------------------------------------------------------------------
--
-- Title       : 	Problema 2.1: Contador
-- Design      :	
-- Author      :	Pablo Sarabia Ortiz
-- Company     :	Universidad de Nebrija
--------------------------------------------------------------------------------
-- File        : contador.vhd
-- Generated   : 13 February 2022
--------------------------------------------------------------------------------
-- Description : Contador síncrono que realiza la siguiente secuencia 
-- 1000, 0111, 0110, 0101, 0100, 0011, sólo cuando enable está a 1
-- LD carga el valor de 1000
-- RST asíncrono a nivel bajo (salida END a 0 y Q a 1000)
--  
--------------------------------------------------------------------------------
-- Revision History :
-- -----------------------------------------------------------------------------

--   Ver  :| Author            :| Mod. Date :|    Changes Made:

--   v1.0  | Pablo Sarabia     :| 28/02/22  :| First version

-- -----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity tb_contador is 
end tb_contador;

architecture testBench of tb_contador is
	component contador is
		port (clk, n_rst, enable, LD: in std_logic;
			finish: out std_logic;
			Q: out std_logic_vector (3 downto 0)
		);
	end component;
	signal clk, n_rst, enable, LD, finish : std_logic := '0';
	signal Q: std_logic_vector(3 downto 0);
	constant clk_period : time:= 20ns;
	
begin
	UUT : contador
	port map(
		clk		=> 	clk,
		n_rst 	=>	n_rst,
		enable 	=> 	enable,
		LD 		=> 	LD,
		finish 	=> 	finish,
		Q 		=>	Q
	);
	clk <= not clk after clk_period/2;
	process is 
	begin
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		n_rst <= '0';
		wait until rising_edge(clk);
		n_rst <= '1';
		wait until rising_edge(clk);
		enable <= '1';
		for i in 0 to 10 loop
			wait until rising_edge(clk);
		end loop;
		enable <= '0';
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		LD <= '1';
		wait until rising_edge(clk);
		enable <= '1';
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		LD <= '0';
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		report "Finished" severity failure; 
	end process;
end testBench;
		