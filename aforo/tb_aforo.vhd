--------------------------------------------------------------------------------
--
-- Title       : 	Problema 5: tn_Aforo_controller
-- Design      :	
-- Author      :	Pablo Sarabia Ortiz
-- Company     :	Universidad de Nebrija
--------------------------------------------------------------------------------
-- File        : tb_aforo_controller.vhd
-- Generated   : 14 February 2022
--------------------------------------------------------------------------------
-- Description : 
--------------------------------------------------------------------------------
-- Revision History :
-- -----------------------------------------------------------------------------

--   Ver  :| Author            :| Mod. Date :|    Changes Made:

--   v1.0  | Pablo Sarabia     :| 14/02/22  :| First version

-- -----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity tb_aforo is
end tb_aforo;

architecture testBench of tb_aforo is
component aforo_controller is
port(clk, rst, i_inc, i_dec: in std_logic;
  i_aforo: in std_logic_vector(7 downto 0);
  o_ocupa: out std_logic_vector(7 downto 0);
  o_bajo, o_medio, o_alto, o_error: out std_logic
);
end component;

signal clk, rst, i_inc, i_dec, o_bajo, o_medio, o_alto, o_error :std_logic := '0';
signal i_aforo, o_ocupa: std_logic_vector(7 downto 0);
constant clk_period : time := 20 ns;
begin
  aforo_controller_inst: aforo_controller
    port map (
      clk     => clk,
      rst     => rst,
      i_inc   => i_inc,
      i_dec   => i_dec,
      i_aforo => i_aforo,
      o_ocupa => o_ocupa,
      o_bajo  => o_bajo,
      o_medio => o_medio,
      o_alto  => o_alto,
      o_error => o_error
    );

    clk <= not clk after clk_period/2;
  process is
  begin
    i_inc   <= '0';
    i_dec   <= '0';
    i_aforo <= (others=>'0');
    wait until rising_edge(clk);
    rst <= '0' ;
    wait until rising_edge(clk);
    rst <= '1';
    i_aforo <= std_logic_vector(to_unsigned(0, i_aforo'length));
    i_inc <= '0';
    i_dec <= '0';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    i_aforo <= std_logic_vector(to_unsigned(10, i_aforo'length));
    i_inc <= '0';
    i_dec <= '1';
    for I in 0 to 12 loop
      wait until rising_edge(clk);
      i_inc <= '1';
      i_dec <= '0';
    end loop;
    for I in 0 to 4 loop
      wait until rising_edge(clk);
      i_inc <= '0';
      i_dec <= '1';
    end loop;
    wait until rising_edge(clk);
    i_inc <= '1';
    i_dec <= '0';
    wait until rising_edge(clk);
    i_aforo <= std_logic_vector(to_unsigned(1, i_aforo'length));
    i_inc <= '1';
    i_dec <= '0';
    wait until rising_edge(clk);
    i_inc <= '1';
    i_dec <= '0';
     wait until rising_edge(clk);
    for I in 0 to 5 loop
      wait until rising_edge(clk);
      i_inc <= '0';
      i_dec <= '1';
    end loop;
    wait until rising_edge(clk);
    i_aforo <= std_logic_vector(to_unsigned(255, i_aforo'length));
    wait until rising_edge(clk);
    for I in 0 to 260 loop
      wait until rising_edge(clk);
      i_inc <= '1';
      i_dec <= '0';
    end loop;
    wait until rising_edge(clk);
    assert false report "Test Finished" severity failure;
    
  end process;
end testBench;
