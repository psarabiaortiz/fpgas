--------------------------------------------------------------------------------
--
-- Title       : 	Problema 5: Aforo_controller
-- Design      :	
-- Author      :	Pablo Sarabia Ortiz
-- Company     :	Universidad de Nebrija
--------------------------------------------------------------------------------
-- File        : aforo_controller.vhd
-- Generated   : 13 February 2022
--------------------------------------------------------------------------------
-- Description : 
--------------------------------------------------------------------------------
-- Revision History :
-- -----------------------------------------------------------------------------

--   Ver  :| Author            :| Mod. Date :|    Changes Made:

--   v1.0  | Pablo Sarabia     :| 13/02/22  :| First version

-- -----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity aforo_controller is
port(clk, rst, i_inc, i_dec: in std_logic;
  i_aforo: in std_logic_vector(7 downto 0);
  o_ocupa: out std_logic_vector(7 downto 0);
  o_bajo, o_medio, o_alto, o_error: out std_logic
);
end aforo_controller;
architecture Behavioral of aforo_controller is 
-- Internal signals to increment or derement the counter of the ocupancy
signal internal_increment, internal_decrement, internal_error : std_logic ; 
-- Counter with the ocupancy
signal cnt : unsigned(7 downto 0);
-- Signals for the thresholds
signal thres_medium, thres_high: unsigned (7 downto 0);  
begin
-- -----------------------Process with the counter operatons--------------------     
process(clk, rst)
    
begin 
    if rst = '0' then
        cnt <= (others => '0');
    elsif rising_edge (clk)then
        if (internal_increment = '1')then
            cnt <= cnt +1;
        elsif ((internal_decrement  = '1' )and (cnt > to_unsigned(0,cnt'length) )) then 
            cnt <= cnt -1;      
        end if;
    end if;    
end process;  
-- -----------------------FSM next state combinational logic--------------------
--(It only has one state so no next state logic required only to internal_increment
-- or internal_decrement the counter)
process(i_aforo, cnt, i_dec, i_inc)
begin
o_error <= '0';
internal_decrement  <= '0';
internal_increment  <= '0';
if unsigned(i_aforo) = to_unsigned(0, i_aforo'length ) then
    o_error <= '1';
    internal_decrement  <= '0';
    internal_increment  <= '0';
else
    o_error <= '0';
    if (unsigned(i_aforo) <= cnt )then
        o_error <=  '1';
        if (i_dec = '1') then
            if (i_inc = '1') then
                internal_decrement  <= '0';
                internal_increment  <= '0';
            else
                internal_decrement <= '1';
                internal_increment  <= '0';
            end if;
        end if;
    else
        if (i_dec = '1') then
            if i_inc = '1' then
                --Nothing to do if increment and decrement pushed
                internal_decrement  <= '0';
                internal_increment  <= '0';
            else
                internal_decrement <= '1';
                internal_increment  <= '0';
            end if;
        else 
             if (i_inc = '1') then
                internal_decrement  <= '0';
                internal_increment <= '1';
             end if;
        end if;
    end if;

end if;
end process;

  --OUTPUT updates
process(cnt, thres_medium, thres_high)
  begin
    
    if cnt<= thres_medium then
    -- Process to update the thresholds signals alto, bajo, medio
      o_bajo <= '1';
      o_medio <= '0';
      o_alto <= '0';
    elsif cnt<= thres_high then
      o_bajo <= '0';
      o_medio <= '1';
      o_alto <= '0';
    else
      o_bajo <= '0';
      o_medio <= '0';
      o_alto <= '1';
    end if;
  -- Connect the output of the occupancy to the counter (CNT)
  
  
  end process;
thres_high <= shift_right(unsigned (i_aforo ), 1);
thres_medium <= shift_right (unsigned(i_aforo), 2);
o_ocupa <= std_logic_vector(cnt);
  
end Behavioral;
