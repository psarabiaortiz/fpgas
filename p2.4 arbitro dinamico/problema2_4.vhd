--------------------------------------------------------------------------------
--
-- Title       : 	Problema 2.4 arbitro cambio de prioridad 
-- Design      :	
-- Author      :	Pablo Sarabia Ortiz
-- Company     :	Universidad de Nebrija
--------------------------------------------------------------------------------
-- File        : Problema2_4.vhd
-- Generated   : 06 April 2022
--------------------------------------------------------------------------------
-- Description : Problema 2.4 Arbitro prioridad dinamica
-- Enunciado   :
-- Se desea crear un arbitro para dos subsistemas sub0 y sub1 que cuando 
-- soliciten el recurso mediante r0, r1 (respectivamente) les de la prioridad de 
-- forma alternativa, mediante g0 y g1. El modo de funcionamiento es el siguiente: 
-- 	Se da la prioridad por defecto al sub1, a partir de ese momento la prioridad
--	se otorga al subsistema que no haya accedido en ultimo lugar.
--------------------------------------------------------------------------------
-- Revision History :
-- -----------------------------------------------------------------------------

--   Ver  :| Author            :| Mod. Date :|    Changes Made:

--   v1.0  | Pablo Sarabia     :| 06/04/22  :| First version

-- -----------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity dyn_prio_arbiter is
	port (
		r0, r1, clk, rst_n	:in std_logic; --rst synchronous low level
		g0, g1				:out std_logic
	);
end dyn_prio_arbiter;

architecture rtl of dyn_prio_arbiter is
	type t_fsm_states is (IdleP1, IdleP0, GrantP0, GrantP1);
	signal current_state, next_state : t_fsm_states;
begin
	fsm_register: process(clk)
	begin
		if rising_edge(clk) then
			if rst_n = '0' then
				current_state <= IdleP1; --default prio to subsystem 1
			else
				current_state <= next_state;
			end if;
		end if;
	end process;
	
	fsm_combinational: process(r0, r1, current_state)
	begin
		-- Default values for the outputs is 0
		g0 <= '0';
		g1 <= '1';
		next_state <= current_state;
		case current_state is
			when IdleP1 =>
				if r1 = '1' then
					next_state <= GrantP1;
				else
					if r0 = '1' then
						next_state <= GrantP0;
					end if;
				end if;
			when GrantP1 =>
				g1 <= '1';
				if r1 = '1' then
					next_state <= GrantP1;
				else
					if r0 = '1' then
						next_state <= GrantP0;
					else
						next_state <= IdleP0;
					end if;
				end if;
			when IdleP0 =>
				if r0 = '1' then
					next_state <= GrantP0;
				else
					if r1 = '1' then
						next_state <= GrantP1;
					end if;
				end if;
			when GrantP0 =>
				g0 <= '1';
				if r0 = '1' then
					next_state <= GrantP0;
				else
					if r1 = '1' then
						next_state <= GrantP0;
					else
						next_state <= IdleP0;
					end if;
				end if;
			
		end case;
	end process;
end rtl;